import os
import webapp2
import jinja2
import urllib2
from xml.dom import minidom
from google.appengine.ext import db

template_dir = os.path.join(os.path.dirname(__file__), 'templates')
jinja_env = jinja2.Environment(loader = jinja2.FileSystemLoader(template_dir), autoescape=True)

class Handler(webapp2.RequestHandler):
    def write(self, *a, **kw):
        self.response.out.write(*a, **kw)
    def render_str(self, template, **params):
        t = jinja_env.get_template(template)
        return t.render(params)
    def render(self, template, **kw):
        self.write(self.render_str(template, **kw))

def get_coords(ip):
	url = "http://api.hostip.info/?ip="+ ip
    	content = None
    	try:
		content = urllib2.urlopen(url).read()
	except:
		return

	if content:
		d = minidom.parseString(content)
		coords = d.getElementsByTagName("gml:coordinates")
		if coords and coords[0].childNodes[0].nodeValue:
			lon,lat = coords[0].childNodes[0].nodeValue.split(',')
			return db.GeoPt(lat,lon)
		
def gmaps_img(points):
    GMAPS_URL = "http://maps.googleapis.com/maps/api/staticmap?size=380x263&sensor=false&"
    for p in points:
        if points.index(p)== (len(points)-1):
            GMAPS_URL += "markers=%s,%s" % (p.lat, p.lon)
        else:
            GMAPS_URL += "markers=%s,%s&" % (p.lat, p.lon)
    return GMAPS_URL

class Art(db.Model):
    title = db.StringProperty(required = True)
    art = db.TextProperty(required = True)
    created = db.DateTimeProperty(auto_now_add = True)
    coords = db.GeoPtProperty()

class ASCIIPage(Handler):
    def render_front(self, title="", art="", error=""):
        arts = db.GqlQuery("SELECT * FROM Art ORDER BY created DESC")
        #self.render("front.html", title=title, art=art, error = error, arts = arts)
    	#arts = list(arts)
	points = filter(None, (a.coords for a in arts))

	img_url = None
	if points:
		img_url = gmaps_img(points)
	
	self.render('front.html', title = title, art=art, error = error, arts = arts, img_url=img_url)

    def get(self):
	#self.write(self.request.remote_addr)
	#self.write(repr(get_coords("4.2.2.2")))
        self.render_front()

    def post(self):
        title = self.request.get("title")
        art = self.request.get("art")

        if title and art:
            a = Art(title = title, art = art)
	    coords = get_coords(self.request.remote_addr)
	    #coords = get_coords("4.2.2.2")
	    if coords:
		    a.coords = coords
            a.put()
            self.redirect("/asciichan")
        else:
            error = "we need both a title and some artwork!"
            self.render_front(title, art, error)

app = webapp2.WSGIApplication([('/asciichan', ASCIIPage)], debug=True)

